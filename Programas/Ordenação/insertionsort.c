#include <stdio.h>

void troca(int *a, int *b){
	int aux = *a;
	*a = *b;
	*b = aux;
}

void insertionsort(int v[], int n){
	int i, j;
	for(i = 1; i < n; ++i){
		for(j = i - 1; j >= 0, v[j + 1] < v[j]; --j){
			troca(&v[j + 1], &v[j]);
		}
	}
}

int main(){
	int k, n, i;
	printf("Insira a quantidade de casos de teste: ");
	scanf("%d", &k);
	printf("\n");
	while(k--){
		printf("Insira o tamanho do vetor de inteiros: ");
		scanf("%d", &n);
		int vetor[n];
		printf("\n");
		printf("Insira os N inteiros:\n");
		for(i = 0; i < n; ++i){
			scanf("%d", &vetor[i]);
		}
		printf("Vetor original:\n");
		for(i = 0; i < n; ++i){
			printf("%d ", vetor[i]);
		}
		printf("\n");
		insertionsort(vetor, n);
		printf("Vetor ordenado:\n");
		for(i = 0; i < n; ++i){
			printf("%d ", vetor[i]);
		}
		printf("\n");
	}
}