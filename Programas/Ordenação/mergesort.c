#include <stdio.h>
/*	
	Recebe os limites da DIREITA, ESQUERDA  e o MEIO de um vetor
	o vetor v[] é dividido de E->Meio e Meio->D, assim, os elem-
	entos sao comparados e colocados em ordem crescente
*/
void dividir(int e, int d, int m, int v[]){
	int i = e, j = m + 1;
	int aux[d - e + 1], k = 0;
	while(i <= m && j <= d){
		if(v[i] < v[j]){
			aux[k] = v[i];
			++i;
		}
		else{
			aux[k] = v[j];
			++j;
		}
		++k;
	}
	while(j <= d)
		aux[k++] = v[j++];
	while(i <= m)
		aux[k++] = v[i++];

	for(k = e; k <= d; ++k)
		v[k] = aux[k - e];
}

void mergesort(int e, int d, int v[]){
	if(e < d){
		int meio = (d + e) / 2;
		mergesort(e, meio, v);
		mergesort(meio + 1, d, v);
		dividir(e, d, meio, v);
	}
}

int main(){
	int k, n, i;
	printf("Insira a quantidade de casos de teste: ");
	scanf("%d", &k);
	printf("\n");
	while(k--){
		printf("Insira o tamanho do vetor de inteiros: ");
		scanf("%d", &n);
		int vetor[n];
		printf("\n");
		printf("Insira os N inteiros:\n");
		for(i = 0; i < n; ++i){
			scanf("%d", &vetor[i]);
		}
		printf("Vetor original:\n");
		for(i = 0; i < n; ++i){
			printf("%d ", vetor[i]);
		}
		printf("\n");
		mergesort(0, n - 1, vetor);
		printf("Vetor ordenado:\n");
		for(i = 0; i < n; ++i){
			printf("%d ", vetor[i]);
		}
		printf("\n");
	}
}