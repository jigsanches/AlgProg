#include <stdio.h>

void troca(int *a, int *b){
	int aux = *a;
	*a = *b;
	*b = aux;
}

void separa(int e, int d, int m, int v[]){
	int i = e, j = m + 1, k = 0, aux[d - e + 1];
	while(i <= m && j <= d){
		if(v[i] <= v[j])
			aux[k++] = v[i++];
		else
			aux[k++] = v[j++];
	}
	while(i <= m)
		aux[k++] = v[i++];
	while(j <= d)
		aux[k++] = v[j++];
	for(k = e; k <= d; ++k){
		v[k] = aux[k - e];
	}
}

void mergesort(int e, int d, int v[]){
	int m;
	if(e < d){
		m = (e + d) / 2;
		mergesort(e, m, v);
		mergesort(m + 1, d, v);
		separa(e, d, m, v);
	}
}

int main(){
	int k, n, i;
	printf("Insira a quantidade de casos de teste: ");
	scanf("%d", &k);
	printf("\n");
	while(k--){
		printf("Insira o tamanho do vetor de inteiros: ");
		scanf("%d", &n);
		int vetor[n];
		printf("\n");
		printf("Insira os N inteiros:\n");
		for(i = 0; i < n; ++i){
			scanf("%d", &vetor[i]);
		}
		printf("Vetor original:\n");
		for(i = 0; i < n; ++i){
			printf("%d ", vetor[i]);
		}
		printf("\n");
		mergesort(0, n - 1, vetor);
		printf("Vetor ordenado:\n");
		for(i = 0; i < n; ++i){
			printf("%d ", vetor[i]);
		}
		printf("\n");
	}
}