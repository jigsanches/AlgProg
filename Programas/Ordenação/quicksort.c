#include <stdio.h>
#define MAX 100

void troca(int *a, int *b){
	int aux = *a;
	*a = *b;
	*b = aux;
}
int separa(int e, int d, int v[]){
	int p = v[e], i = e + 1, j = d;
	while(i <= j){
		if(v[i] <= p)
			++i;
		else if(v[j] > p)
			--j;
		else
			troca(&v[i++], &v[j--]);
	}
	troca(&v[e], &v[j]);
	return j;
}

void quickSort(int e, int d, int v[]){
	int p;
	if(e < d){
		p = separa(e, d, v);
		quickSort(e, p - 1, v);
		quickSort(p + 1, d, v);
	}
}

int main(){
	int k, n, i;
	printf("Insira a quantidade de casos de teste: ");
	scanf("%d", &k);
	printf("\n");
	while(k--){
		printf("Insira o tamanho do vetor de inteiros: ");
		scanf("%d", &n);
		int vetor[n];
		printf("\n");
		printf("Insira os N inteiros:\n");
		for(i = 0; i < n; ++i){
			scanf("%d", &vetor[i]);
		}
		printf("Vetor original:\n");
		for(i = 0; i < n; ++i){
			printf("%d ", vetor[i]);
		}
		printf("\n");
		quickSort(0, n - 1, vetor);
		printf("Vetor ordenado:\n");
		for(i = 0; i < n; ++i){
			printf("%d ", vetor[i]);
		}
		printf("\n");
	}
}