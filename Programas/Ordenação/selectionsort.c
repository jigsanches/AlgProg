#include <stdio.h>

void troca(int *a, int *b){
	int aux = *a;
	*a = *b;
	*b = aux;
}
/*
	O algoritmo de Seleção pega sempre o maior valor encontrado no vetor e joga pro final
*/
void selectionsort(int n, int v[]){
	int i, j, min;
	for(i = 0; i < n; ++i){
		min = i;
		for(j = i + 1; j < n; ++j){
			if(v[j] < v[min])
				min = j;
		}
		troca(&v[i], &v[min]);
	}
}

int main(){
	int k, n, i;
	printf("Insira a quantidade de casos de teste: ");
	scanf("%d", &k);
	printf("\n");
	while(k--){
		printf("Insira o tamanho do vetor de inteiros: ");
		scanf("%d", &n);
		int vetor[n];
		printf("\n");
		printf("Insira os N inteiros:\n");
		for(i = 0; i < n; ++i){
			scanf("%d", &vetor[i]);
		}
		printf("Vetor original:\n");
		for(i = 0; i < n; ++i){
			printf("%d ", vetor[i]);
		}
		printf("\n");
		selectionsort(n, vetor);
		printf("Vetor ordenado:\n");
		for(i = 0; i < n; ++i){
			printf("%d ", vetor[i]);
		}
		printf("\n");
	}
}