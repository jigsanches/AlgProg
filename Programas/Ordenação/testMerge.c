#include <stdio.h>
/*	
	tentativa de fazer o merge sort com melhoramento:
	a ideia é: fazer com que a segunda parte do vetor (direita) seja invertida na hora das comparações
*/
void troca(int *a, int *b){
	int aux = *a;
	*a = *b;
	*b = aux;
}

void inverte(int e, int d, int v[]){
	int i = e, j = d;
	while(i < j){
		troca(&v[i], &v[j]);
		++i;
		--j;
	}
}

void intercala(int e, int d, int m, int v[]){
	int i = e, j = d, k = 0;
	int aux[d - e + 1];
	inverte(m + 1, d, v);
	while(i <= j){
		if(v[i] < v[j])
			aux[k++] = v[i++];
		else
			aux[k++] = v[j++];
	}
	for(k = e; k <= d; ++k){
		v[k] = aux[k - e];
	}
}

void mergesort(int e, int d, int v[]){
	int meio;
	if(e < d){
		meio = (e + d) / 2;
		mergesort(e, meio, v);
		mergesort(meio + 1, d, v);
		intercala(e, d, meio, v);
	}
}

int main(){
	int n, i;
	scanf("%d", &n);
	int v[n];
	for(i = 0; i < n; ++i){
		scanf("%d", &v[i]);
		printf("%d ", v[i]);
	}
	printf("\n\n");

	mergesort(0, n - 1, v);
	for(i = 0; i < n; ++i){
		printf("%d ", v[i]);
	}
	printf("\n\n");
	return 0;
}