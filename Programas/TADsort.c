#include <stdio.h>
#include <stdlib.h>
#define ASKNUM printf("Insira o número de elementos\n"); scanf("%d", &size);
#define ASKELEM printf("Insira os elementos do vetor separados por ESPAÇO\n");

void readI(int v[], int size){
	int i;
	for(i = 0; i < size; ++i){
		scanf("%d", &v[i]);
	}
}

void readC(char c[], int size){
	int i;
	for(i = 0; i < size; ++i){
		scanf(" %c", &c[i]);
	}
}

void readD(double d[], int size){
	int i;
	for(i = 0; i < size; ++i){
		scanf("%lf", &d[i]);
	}
}

void print1(int *v, int size){
	int i;
	for(i = 0; i < size; ++i){
		printf("%d ", v[i]);
	}
	printf("\n");
}

void print2(char v[], int size){
	int i;
	for(i = 0; i < size; ++i){
		printf("%c ", v[i]);
	}
	printf("\n");
}

void print3(double v[], int size){
	int i;
	for(i = 0; i < size; ++i){
		printf("%lf ", v[i]);
	}
	printf("\n");
}

int comparaI(const void *p1, const void *p2){
	return (*(int *)p1) - (*(int *)p2);
}

int comparaC(const void *p1, const void *p2){
	return (*(char *)p1) - (*(char *)p2);
}

int comparaD(const void *p1, const void *p2){
	return (*(double *)p1) - (*(double *)p2);
}

int main(){
	int caso, size, dataLen;

	int (*compara)(const void *p1, const void *p2);

	printf("Insira o código da operação que você deseja executar:\n");
	printf("1\tordenação de inteiros\n");
	printf("2\tordenação de caracteres\n");
	printf("3\tordenação de double\n");
	printf("0\tterminar execução\n");

	scanf("%d", &caso);
	while(caso){
		void *v;
			if(caso == 1){
				ASKNUM
				*(int *) v;
				ASKELEM
				readI((int *)v, size);
				print1(v, size);
				compara = &comparaI;
				dataLen = sizeof(int);
			}
/*
			else if(caso == 2){
				ASKNUM
				ASKELEM
				readC((char *)v, size);
				print2(v, size);
				compara = &comparaC;
				dataLen = sizeof(char);
			}

			else if(caso == 3){
				ASKNUM
				ASKELEM
				readD((double *)v, size);
				print3(v, size);
				compara = &comparaD;
				dataLen = sizeof(double);
			}
*/			qsort(v, size, dataLen, compara);

			if 		(caso == 1) 		print1(v, size);
			else if (caso == 2)			print2(v, size);
			else if (caso == 3)			print3(v, size);

			scanf("%d", &caso);
		}
	return 0;
}