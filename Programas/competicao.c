#include <stdio.h>

int calculaBusca(int n, int m){
	int meio, lMax = n - 1, lMin = 0, i = 0;
	while((lMax + lMin) / 2 != m){
		meio = (lMin + lMax) / 2;
		if(meio == m){
			break;
		}
		if(meio > m){
			lMax = meio - 1;
			++i;
		}
		else if(meio < m){
			lMin = meio + 1;
			++i;
		}
	}
	return i;
}

int main(){
	/*	
		N representa o número de palavras
		M representa o número da palavra que buscamos
		T1 é o tempo de busca vetorial/linear
		T2 é o tempo da busca binária
	*/
	int k, n, m, t1, t2;
	int perf;
	scanf("%d", &k);
	while(k--){
		scanf("%d%d%d%d", &n, &m, &t1, &t2);
		perf = 1 + calculaBusca(n, m - 1);
		if(m * t1 == perf * t2)
			printf("0\n");
		else if(m * t1 < perf * t2)
			printf("1\n");
		else
			printf("2\n");
	}
	return 0;
}