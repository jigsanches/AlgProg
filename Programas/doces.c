#include <stdio.h>
#define false 0
#define true 1

int pai(int n){
	if(n == 0)
		return 0;
	return (n - 1) / 2;
}

int fesq(int i){
	return (2 * i) + 1;
}

int fdir(int i){
	return (2 * i) + 2;
}

void desce(int heap[], int n, int i){
	int e, d, imax;
	e = fesq(i);
	d = fdir(i);
	imax = i;
	if(e < n && heap[e] < heap[imax])
		imax = e;
	if(d < n && heap[d] < heap[imax])
		imax = d;
	if(imax != i){
		int aux = heap[imax];
		heap[imax] = heap[i];
		heap[i] = aux;
		desce(heap, n, imax);
	}
}

void makeHeap(int heap[], int n){
	int i;
	for(i = (n - 1) /2 ; i >= 0; --i){
		desce(heap, n, i);
	}
}


int main(){
	int n, k, i, quantidade = 0, foiPossivel = true;
	scanf("%d%d", &n, &k);
	int heap[n];
	for(i = 0; i < n; ++i){
		scanf("%d", &heap[i]);
	}
	while(heap[0] < k){
		if(n <= 1){
			foiPossivel = false;
			break;
		}
		++quantidade;
		heap[1] = heap[0] + heap[1] * 2;
		heap[0] = heap[n - 1];
		--n;
		makeHeap(heap, n);
	}
	if(foiPossivel)
		printf("%d\n", quantidade);
	else
		printf("-1\n");
}