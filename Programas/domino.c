#include <stdio.h>
#define MAX 14

struct dominoes{
	int esq;
	int dir;
	int usado;
};

void leiaPecas(struct dominoes pecas[MAX], int m){
	int i;
	for(i = 0; i < m; ++i){
		pecas[i].usado = 0;
		scanf("%d%d", &pecas[i].esq, &pecas[i].dir);
	}
}

void troca(struct dominoes *peca){
	int aux = peca->esq;
	peca->esq = peca->dir;
	peca->dir = aux;
}

int solitaria(struct dominoes atual, struct dominoes final, int quantidade, int lacunas, struct dominoes pecas[MAX]){
	int i;
	if(lacunas == 0){
		if(final.esq == atual.dir)
			return 1;
	}
	else{
		for(i = 0; i < quantidade; ++i){
			if(!pecas[i].usado){
				if(atual.dir == pecas[i].esq){
					pecas[i].usado = 1;
					if(solitaria(pecas[i], final, quantidade, lacunas - 1, pecas))
						return 1;
					pecas[i].usado = 0;
				}
				else if(atual.dir == pecas[i].dir){
					troca(&pecas[i]);
					pecas[i].usado = 1;
					if(solitaria(pecas[i], final, quantidade, lacunas - 1, pecas))
						return 1;
					pecas[i].usado = 0;
				}
			}
		}
	}
	return 0;
}

int main(){
	int n, m;
	struct dominoes pecas[MAX];
	struct dominoes inicial;
	struct dominoes final;
	while(1){
		scanf("%d", &n);
		if(!n)	break;
		scanf("%d", &m);
		scanf("%d%d", &inicial.esq, &inicial.dir);
		/*leitura das pecas iniciais e finais*/
		scanf("%d%d", &final.esq, &final.dir);
		leiaPecas(pecas, m);
		if(solitaria(inicial, final, m, n, pecas))
			printf("YES\n");
		else
			printf("NO\n");

	}
	return 0;
}