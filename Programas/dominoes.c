#include <stdio.h>

#define TRUE 1
#define FALSE 0

typedef struct {
	int left;
	int right;
	int used;
} domino;

void swap(domino *piece) {
	int aux = piece->left;
	piece->left = piece->right;
	piece->right = aux;
}

int combination(domino previous, domino final, domino pieces[], int numberOfPieces, int blankSpaces) {
	int i;

	if (blankSpaces == 0) {
		return previous.right == final.left;
		
		//if (previous.right == final.left) {
		//	printf("YES\n");
		//} else {
		//	printf("NO\n");
		//}
	} else {
		int numberToMatch = previous.right;
		
		for (i = 0; i < numberOfPieces; i++) {
			domino current = pieces[i];

			int isUsedBefore = current.used;
			int leftValue = current.left;
			int rightValue = current.right;
			
			if (!isUsedBefore) {
				if (numberToMatch == leftValue || numberToMatch == rightValue) {
					if (numberToMatch == rightValue) swap(&pieces[i]);
					pieces[i].used = TRUE;
					
					int hasCombination = combination(pieces[i], final, pieces, numberOfPieces, blankSpaces - 1);
					if (hasCombination) return TRUE;

					pieces[i].used = FALSE;			
					if (numberToMatch == rightValue) swap(&pieces[i]);
				}
			}
		}

		return FALSE;
	}
}

domino readPiece() {
	domino piece;
	piece.used = FALSE;		

	scanf("%d %d", &piece.left, &piece.right);

	return piece;
}

void readPieces(domino pieces[], int numberOfPieces) {
	int i = 0;
	for (; i < numberOfPieces; i++) {
		pieces[i] = readPiece();
	}
}

int main() {
	int n, m;

	while(1) {
		scanf("%d", &n);
		if (n == 0) break;
		
		scanf("%d", &m);
		domino pieces[m];

		domino initial = readPiece();
		domino final   = readPiece();
		
		readPieces(pieces, m);

		int hasCombination = combination(initial, final, pieces, m, n);

		if (hasCombination) {
			printf("YES\n");
		} else {
			printf("NO\n");
		}
	}
 
	return 0;
}
