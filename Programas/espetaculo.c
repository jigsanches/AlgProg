#include <stdio.h>
#include <stdlib.h>

typedef struct node{
	int reg;
	struct node *prox;
}tnode;

void insere(int c, tnode **p){
	tnode *inc = *p;
	if(*p == NULL){
		inc = malloc(sizeof(tnode));
		inc->reg = c;
		inc->prox = *p;
		*p = inc;
		return;
	}
	while(inc->prox != NULL)
	{
		inc = inc->prox;
	}
	tnode *new = malloc(sizeof(tnode));
	new->reg = c;
	new->prox = inc->prox;
	inc->prox = new;
}

void removeFila(tnode **p){
	if(*p == NULL)
	{
		return;
	}
	tnode *aux = *p;
	*p = aux->prox;
	free(aux);
}

void printa(tnode **p){
	int i = 0;
	if(*p == NULL)
	{
		return;
	}
	tnode *aux = *p;
	while(aux->prox != NULL)
	{
		if(i++ == 2) break;
		printf("%d ",aux->reg);
		aux = aux->prox;
	}
	printf("%d ", aux->reg);
}

int main(){
	int ingressos, idade, k = 1, sessao = 1, i;
	scanf("%d", &ingressos);
	tnode *filaC = NULL;
	tnode *filaA = NULL;
	tnode *filaI = NULL;
	while(ingressos--){
		scanf("%d", &idade);
		if(idade < 18)
		{
			insere(k, &filaC);
			++k;
		}
		else if(idade < 41)
		{
			insere(k, &filaA);
			++k;
		}
		else
		{
			insere(k, &filaI);
			++k;
		}
	}
	while(filaI != NULL || filaA != NULL || filaC != NULL){
		printf("sessao %d:\n", sessao++);
		printa(&filaC);
		printa(&filaA);
		printa(&filaI);
		printf("\n");
		for(i = 0; i < 3; ++i){
			removeFila(&filaC);
			removeFila(&filaA);
			removeFila(&filaI);
		}
	}
	return 0;
}
