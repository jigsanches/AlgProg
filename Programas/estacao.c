#include <stdio.h>
#include <stdlib.h>

typedef struct node{
	int reg;
	struct node *prox;
}tnode;

void push(tnode **p, int c){
	tnode *aux = malloc(sizeof(tnode));
	aux->reg = c;
	aux->prox = *p;
	*p = aux;
}

void pop(tnode **p){
	tnode *aux = *p;
	aux = aux->prox;
	free(*p);
	*p = aux;
}

int main()
{
	int k, n, i, pos = 0;
	scanf("%d", &k);
	while(k--){
		tnode *lista = NULL;
		pos = 0;
		scanf("%d", &n);
		int key[n], conf[n];
		for(i = 0; i < n;++i)
		{
			scanf("%d", &key[i]);
			conf[i] = i + 1;
		}
		for(i = 0; i < n; ++i)
		{
			push(&lista, conf[i]);
			while(lista != NULL && lista->reg == key[pos])
			{
				pop(&lista);
				++pos;
			}
		}
		if(lista == NULL){
			printf("Sim\n");
		}
		else{
			printf("Nao\n");
		}
	}
	return 0;
}