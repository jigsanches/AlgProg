#include <stdio.h>
#include <stdlib.h>

typedef struct node{
	int reg;
	struct node *prox;
}tnode;

void insere(int c, tnode **p){
	tnode *inc = *p;
	if(*p == NULL){
		inc = malloc(sizeof(tnode));
		inc->reg = c;
		inc->prox = *p;
		*p = inc;
		return;
	}
	while(inc->prox != NULL)
	{
		inc = inc->prox;
	}
	tnode *new = malloc(sizeof(tnode));
	new->reg = c;
	new->prox = inc->prox;
	inc->prox = new;
}

void removeFila(tnode **p){
	if(*p == NULL)
	{
		printf("fila vazia\n");
		return;
	}
	tnode *aux = *p;
	*p = aux->prox;
	free(aux);
}

void printa(tnode **p){
	if(*p == NULL)
	{
		printf("fila vazia\n");
		return;
	}
	tnode *aux = *p;
	while(aux->prox != NULL)
	{
		printf("%d ",aux->reg);
		aux = aux->prox;
	}
	printf("%d\n", aux->reg);
}


int main(){
	int comando, k;
	scanf("%d", &comando);
	tnode *fila = NULL;
	while(comando){
		if 		(comando == 1)
		{
			scanf("%d", &k);
			insere(k, &fila);
		}
		else if (comando == 2)
		{
			removeFila(&fila);
		}
		else if (comando == 3)
		{
			printa(&fila);
		}
		scanf("%d", &comando);
	}
}