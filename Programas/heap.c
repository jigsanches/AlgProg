#include <stdio.h>

int pai(int n){
	if(n == 0)
		return 0;
	return (n - 1) / 2;
}

int fesq(int i){
	return (2 * i) + 1;
}

int fdir(int i){
	return (2 * i) + 2;
}

void desce(int heap[], int n, int i){
	int e, d, imax;
	e = fesq(i);
	d = fdir(i);
	imax = i;
	if(e < n && heap[e] > heap[imax])
		imax = e;
	if(d < n && heap[d] > heap[imax])
		imax = d;
	if(imax != i){
		int aux = heap[imax];
		heap[imax] = heap[i];
		heap[i] = aux;
		desce(heap, n, imax);
	}
}

void makeHeap(int heap[], int n){
	int i;
	for(i = (n - 1) /2 ; i >= 0; --i){
		desce(heap, n, i);
	}
}

void printHeap(int heap[], int n){
	int qLinha = 0, i;
	if(n == 0){
		printf("vazio\n");
		return;
	}
	for(i = 0; i < n; ++i){
		printf("%d ", heap[i]);
		if(i == qLinha){
			printf("\n");
			qLinha += i + 2;
		}
	}
	//ta dando certo a aritmetica ta linda e majestosa :^)
	if(n - 1 != (qLinha - 2) / 2)
		printf("\n");
}

int main(){
	int comando;
	int heap[100], tamanho = 0;
	scanf("%d", &comando);
	while(comando){
		switch(comando){
			case 1 :
				scanf("%d", &heap[tamanho++]);
				makeHeap(heap, tamanho);
			break;

			case 2 :
				heap[0] = heap[tamanho - 1];
				--tamanho;
				makeHeap(heap, tamanho);
				//remove a raiz da estrutura|||| isso é feito colocando o ultimo valor no primeiro lugar (heap[0]) e chama a função ;desce;
				//tamanho--
			break;

			case 3 :
				printHeap(heap, tamanho);
			break;

		}
		scanf("%d", &comando);
	}
}