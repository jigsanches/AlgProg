#include <stdio.h>
#include <stdlib.h>

typedef struct damn{
	int content;
	struct damn *prox;
} celula;
//insere um valor na lista usando a célula anterior como referencial e retorna o endereço da ultima célula criada
void insereC(int x, celula *p){
	celula *nova = malloc(sizeof(celula));
	nova->content = x;
	nova->prox = p->prox;
	p->prox = nova;
}
void insereF(int x, celula *p){
	while(p->prox != NULL){
		p = p->prox;
	}
	celula *nova = malloc(sizeof(celula));
	nova->content = x;
	nova->prox = p->prox;
	p->prox = nova;
}
void removeC(celula *p){
	if(p->prox == NULL){
		printf("lista vazia\n");
		return;
	}
	p->prox = p->prox->prox;
}
void removeF(celula *p){
	if(p->prox == NULL){
		printf("lista vazia\n");
		return;
	}
	while(p->prox->prox != NULL){
		p = p->prox;
	}
	p->prox = NULL;
}
void printaLista(celula *p){
	if(p->prox == NULL){
		printf("lista vazia\n");
		return;
	}
	p = p->prox;
	while(p->prox != NULL){
		printf("%d ", p->content);
		p = p->prox;
	}
	printf("%d\n", p->content);
}



int main(){
	int comando, valor;
	celula cabeca;
	cabeca.prox = NULL;
	scanf("%d", &comando);
	while(comando != 0){
		if(comando == 1){
			scanf("%d", &valor);
			insereC(valor, &cabeca);
		}
		else if(comando == 2){
			scanf("%d", &valor);
			insereF(valor, &cabeca);
		}
		else if(comando == 3){
			removeC(&cabeca);
		}
		else if(comando == 4){
			removeF(&cabeca);
		}
		else if(comando == 5){
			printaLista(&cabeca);
		}
		scanf("%d", &comando);
	}

	return 0;
}