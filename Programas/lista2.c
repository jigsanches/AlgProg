#include <stdio.h>
#include <stdlib.h>

typedef struct node{
	int cel;
	struct node * prox;
}tnode;

void insereC(tnode **v, int c){
	tnode *nova = malloc(sizeof(tnode));
	nova->cel = c;
	nova->prox = *v;
	*v = nova;
}

void insereF(tnode **v, int c){
	tnode *aux = *v;
	while(aux->prox != NULL){
		aux = aux->prox;
	}
	tnode *nova = malloc(sizeof(tnode));
	nova->cel = c;
	nova->prox = aux->prox;
	aux->prox = nova;
}

void removeC(tnode **v){
	tnode *aux = (*v)->prox->prox;
	free((*v)->prox);
	*v = aux;
}

void removeF(tnode **v){
	tnode *aux = *v;
	while(aux->prox->prox != NULL){
		aux = aux->prox;
	}
	free(aux->prox);
	aux->prox = NULL;
}

void printaLista(tnode **v){
	tnode *aux = *v;
	/*if(aux->prox == NULL){
		printf("lista vazia\n");
		return;
	}*/
	while(aux->prox != NULL){
		printf("%d ", aux->cel);
		aux = aux->prox;
	}
	printf("%d\n", aux->cel);
}

int main(){
	int comando, valor;
	scanf("%d", &comando);
	while(comando != 0){
		tnode *lista;
		lista = NULL;
		if(comando == 1){
			scanf("%d", &valor);
			insereC(&lista, valor);
		}
		else if(comando == 2){
			scanf("%d", &valor);
			insereF(&lista, valor);
		}
		else if(comando == 3){
			removeC(&lista);
		}
		else if(comando == 4){
			removeF(&lista);
		}
		else if(comando == 5){
			printaLista(&lista);
		}
		scanf("%d", &comando);
	}

	return 0;
}