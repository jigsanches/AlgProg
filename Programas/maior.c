#include <stdio.h>

void leiaVetor(int n, int v[]){
	int i;
	for(i = 0; i < n; ++i){
		scanf("%d", &v[i]);
	}
}

/*MANO QUE PROBLEMA É ESSE É IMPOSSÍVEL PROFESSOR!!!!!!!!
ME AJUDA												*/
int busca(int n, int v[], int *e, int *d){
	int lMax = n - 1, lMin = 0;
	int meio;
	while(lMax > lMin - 1){
		meio = (lMin + lMax) / 2;
		if(meio > 0 && v[meio] < v[meio - 1]){
			lMax = meio - 1;
			*e = *e + 1;
		}
		else if(meio < n - 1 && v[meio] < v[meio + 1]){
			lMin = meio + 1;
			*d = *d + 1;
		}
		else if(lMax > lMin - 1){
			return v[meio];
		}
	}
	return 0;
}

int main(){
	int k, n, v[1000], maior;
	int e, d;
	scanf("%d", &k);
	while(k-- > 0){
		scanf("%d", &n);
		e = 0;
		d = 0;
		leiaVetor(n, v);
		maior = busca(n, v, &e, &d);
		printf("movimentos para a esquerda: %d\nmovimentos para a direita: %d\nmaior elemento: %d\n\n", e, d, maior); 
	}
	return 0;
}
