#include <stdio.h>

void troca(char *a, char *b){
	char aux = *a;
	*a = *b;
	*b = aux;
}

void permuta(char v[], int n, int *c, int j){
	int i;
	if(j == n - 1){
		(*c)--;
		if(*c == 0){
			for(i = 0; i < n; i++){
				printf("%c", v[i]);
			}
			printf("\n");
			return;
		}
	}
	else{
		for(i = j; i < n; ++i){
			troca(&v[i], &v[j]);
			permuta(v, n, c, j + 1);
			troca(&v[i], &v[j]);
		}
	}
}

int main(){
	int k, n, num;
	char vet[10] = "123456789";
	scanf("%d", &k);
	while (k-- > 0){
		scanf("%d%d", &n, &num);
		permuta(vet, n, &num, 0);
	}
	return 0;
}