#include <stdio.h>
#include <stdlib.h>

typedef struct node{
	int reg;
	struct node *prox;
}tnode;

void empilha(tnode **p, int c){
	tnode *aux = malloc(sizeof(tnode));
	aux->reg = c;
	aux->prox = *p;
	*p = aux;
}

void desempilha(tnode **p){
	if(*p == NULL){
		printf("pilha vazia\n");
		return;
	}
	tnode *aux = *p;
	aux = aux->prox;
	free(*p);
	*p = aux;
}

void printaLista(tnode **p){
	if(*p == NULL){
		printf("pilha vazia\n");
		return;
	}
	tnode *aux = *p;
	while(aux->prox != NULL){
		printf("%d ", aux->reg);
		aux = aux->prox;
	}
	printf("%d\n", aux->reg);

}

int main(){
	int comando, value;
	tnode *lista;
	lista = NULL;
	scanf("%d", &comando);
	while(comando){
		//empilha
		if(comando == 1){
			scanf("%d", &value);
			empilha(&lista, value);
		}
		//desempilha
		else if(comando == 2){
			desempilha(&lista);
		}
		//printa
		else if(comando == 3){
			printaLista(&lista);
		}
		scanf("%d", &comando);
	}

	return 0;
}