#include <stdio.h>

int bahia(int v[], int pos, int i){
	int j;
	for(j = 0; j < pos; ++j){
		if(v[j] == i)
			return 0;
		if(pos - j == i - v[j])
			return 0;
		if(v[j] - pos == v[j] - i)
			return 0;
	}
	return 1;
}

void rainhas(int v[], int pos){
	int i;
	if(pos == 8){
		for(i = 0; i < 8;++i)
			printf("%d", v[i]);
		printf("\n");
		return;
	}
	for(i = 0; i < 8; ++i){
		if(bahia(v, pos, i) == 1){
			v[pos] = i + 1;
			rainhas(v, pos+1);
			v[pos] = 0; 
		}
	}
}

int main(){
	int v[8];
	rainhas(v, 0);
	return 0;
}