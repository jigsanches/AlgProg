#include <stdio.h>

int primos[] = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31};

struct numeros{
	int alg;
	int usado;
};

void geraRole(struct numeros entao[], int n){
	int i;
	for(i = 0; i < n; ++i){
		entao[i].alg = i + 1;
		entao[i].usado = 0;
	}
}

void printaVetor(int n, int vetor[]){
	int i;
	for(i = 0; i < n; ++i){
		printf("%d ", vetor[i]);
	}
	printf("\n");
}

int confere(int v1, int v2){
	int i;
	int soma = v1 + v2;
	for(i = 0; i < 10; ++i){
		if(soma == primos[i]){
			return 1;
		}
	}
	return 0;
}

void permutaNum(struct numeros lul[], int vetor[], int n, int pos){
	int i;
	if(pos == n){
		if(confere(vetor[0], vetor[n - 1]))
			printaVetor(n, vetor);
		return;
	}
	for(i = 0; i < n; ++i){
		if(!lul[i].usado){
			if(confere(vetor[pos - 1], lul[i].alg)){
				lul[i].usado = 1;
				vetor[pos] = lul[i].alg;
				permutaNum(lul, vetor, n, pos+1);
				lul[i].usado = 0;
			}
		}
	}
}

int main(){
	int n, k;
	int preencher[16];
	struct numeros geral[15];
	preencher[0] = 1;
	scanf("%d", &k);
	geraRole(geral, 15);
	geral[0].usado = 1;
	while(k--){
		scanf("%d", &n);
		permutaNum(geral, preencher, n, 1);
		printf("\n");
	}

	return 0;
}