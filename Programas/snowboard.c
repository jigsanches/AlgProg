#include <stdio.h>
#define MAX 100

void leia(int m, int n, int A[MAX][MAX]){
	int i, j;
	for(i = 0; i < m; ++i)
		for(j = 0; j < n; ++j)
			scanf("%d", &A[i][j]);
}
/*	a função (BAHIA) recebe a matriz e uma coordenada enviada pela função RUN
	a partir dessa posição [X,Y] ele caminha pelos valores válidos e para cada lado que a primeira chamada
	chamar recursivamente, será guardado o valor da maior distância possível em uma posição do vetor _caminho[4]_
	no fim, os 4 valores são comparados e o maior deles é retornado para a função RUN.
	Desta forma, a cada chamada desta função é retornado o valor do maior trajeto possível a partir da posição [X,Y]
*/

/*
1 2 3 4 5
16 17 18 19 6
15 24 25 20 7
14 23 22 21 8
13 12 11 10 9*/

int bahia(int matriz[MAX][MAX], int m, int n, int x, int y){
	int atual = matriz[x][y], i;
	int caminho[4] = {0};

	if(x > 0 && matriz[x - 1][y] < atual)
		caminho[0] = bahia(matriz, m, n, x - 1, y);
	if(x < m - 1 && matriz[x + 1][y] < atual)
		caminho[1] = bahia(matriz, m, n, x + 1, y);
	if(y > 0 && matriz[x][y - 1] < atual)
		caminho[2] = bahia(matriz, m, n, x, y - 1);
	if(y < n - 1 && matriz[x][y + 1] < atual)
		caminho[3] = bahia(matriz, m, n, x, y + 1);

	int maiorValor = caminho[0];
	for (i = 1; i < 4; ++i){
		if(caminho[i] > maiorValor)
			maiorValor = caminho[i];
	}
	return maiorValor + 1;

}

/*anda na matriz e passa coordenadas X e Y para a função principal (BAHIA)*/
int run(int matriz[MAX][MAX], int m, int n){
	int i, j, maiorValor = 0, valorAtual;
	for(i = 0; i < m; ++i){
		for(j = 0; j < n; ++j){
			valorAtual = bahia(matriz, m, n, i, j);
			if(valorAtual > maiorValor)
				maiorValor = valorAtual;
		}
	}
	return maiorValor;
}


int main(){
	int k, n, m;
	int matriz[MAX][MAX];
	scanf("%d", &k);
	while(k-- > 0){
		scanf("%d%d", &m, &n);
		leia(m, n, matriz);
		printf("%d\n", run(matriz, m, n));
	}

	return 0;
}