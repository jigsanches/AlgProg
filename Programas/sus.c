#include <stdio.h>
#include <stdlib.h>

typedef struct node{
	int chegada;
	int esp;
	struct node *prox;
}tnode;

void insere(int chega, int wait, tnode **p){
	tnode *inc = *p;
	if(*p == NULL){
		inc = malloc(sizeof(tnode));
		inc->chegada = chega;
		inc->esp = wait;
		inc->prox = *p;
		*p = inc;
		return;
	}
	while(inc->prox != NULL)
	{
		inc = inc->prox;
	}
	tnode *new = malloc(sizeof(tnode));
	new->chegada = chega;
	new->esp = wait;
	new->prox = inc->prox;
	inc->prox = new;
}

void removeFila(tnode **p){
	if(*p == NULL)
	{
		return;
	}
	tnode *aux = *p;
	*p = aux->prox;
	free(aux);
}

int converte(int hora){
	return hora * 60;
}

int main(){
	int tempoAt = 0, horaP, minP, espera, n, critico = 0, k;
	tnode *lista = NULL;
	while(scanf("%d", &n) != EOF){
		for(k = 0; k < n; ++k){
			scanf("%d%d%d", &horaP, &minP, &espera);
			minP = minP + converte(horaP - 7);
			insere(minP, espera, &lista);
		}
		while(lista != NULL){
			if(tempoAt > lista->chegada + lista->esp){
				critico++;
				tempoAt += 30;
			}
			else if(tempoAt == lista->chegada + lista->esp){
				tempoAt += 30;
			}
			else{
				tempoAt = lista->chegada + 30;
			}
			removeFila(&lista);
		}
		printf("%d\n", critico);
		tempoAt = 0;
		critico = 0;
	}
	return 0;
}