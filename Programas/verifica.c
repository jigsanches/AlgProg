#include <stdio.h>
#define true 1
#define false 0

int pai(int n, int filhos){
	return (n - 1) / filhos;
}
// passe (k - 1)
int verifica(int v[], int k, int filhos){
	int i, iPai;
	for(i = 1; i <= k; ++i){
		iPai = pai(i, filhos);
		if(v[iPai] > v[i])
			return 0;
	}
	return 1;
}
void printHeap(int heap[], int n){
	int qLinha = 0, i;
	if(n == 0){
		printf("vazio\n");
		return;
	}
	for(i = 0; i < n; ++i){
		printf("%d ", heap[i]);
		if(i == qLinha){
			printf("\n");
			qLinha += i + 2;
		}
	}
	if(n - 1 != qLinha)
		printf("\n");
}

int main(){
	int n, k, i;
	while(1){
		scanf("%d", &n);
		if(n == 0)
			break;
		scanf("%d", &k);
		int v[k];
		for(i = 0; i < k; ++i){
			scanf("%d", &v[i]);
		}
		printHeap(v, k);
		(verifica(v, k - 1, n)) ? printf("sim\n") : printf("nao\n");
	}
}